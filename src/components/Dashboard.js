import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faSearch, faLevelUpAlt, faLevelDownAlt, faSortUp, faSortDown, faUserAlt, faChevronDown, faLongArrowAltUp } from "@fortawesome/free-solid-svg-icons";
import { Line, Doughnut } from 'react-chartjs-2';

export default class Dashboard extends React.Component {
  render() {
    let stats = this.props.functions.getStats();
    stats.sort((a, b) => { return a.month - b.month; })
    let dataCurr = stats[stats.length - 1].data;

    let extraCurr = this.getExtraData(1);
    let extraLast = this.getExtraData(2);

    let labels = dataCurr.reduce((acc, curr) => { acc.push(curr["day"]); return acc; }, []);
    let userOverviewItems = Array.from(extraCurr.userOverviews.values()).map(userOverview => {
      return this.getUserOverview(userOverview);
    })
    let browserOverviewItems = Array.from(extraCurr.usersByBrowser.values()).map(browserOverview => {
      let lastBrowserOverview = extraLast.usersByBrowser.has(browserOverview.browser) ? extraLast.usersByBrowser.get(browserOverview.browser) : { browser: browserOverview.browser, value: 0 };
      return this.getBrowserOverview(browserOverview, lastBrowserOverview);
    })

    let pcUsers = extraCurr.usersByPlatform.has("pc") ? extraCurr.usersByPlatform.get("pc").value : 0;
    let mobileUsers = extraCurr.usersByPlatform.has("mobile") ? extraCurr.usersByPlatform.get("mobile").value : 0;
    let tabletUsers = extraCurr.usersByPlatform.has("tablet") ? extraCurr.usersByPlatform.get("tablet").value : 0;
    let difference = ((extraCurr.totalUsers - extraLast.totalUsers)/extraLast.totalUsers)*100;

    return (
      <div className="dashboard" style={{ width: "calc(100% - " + (this.props.mainState.sidebarCompactView ? 64 : 300) + "px)"  }}>
        <div className="dashboardSection1">
          <div className="dashboardSection1a">
            <button className="dashboardMenuButton" onClick={(e) => { this.props.functions.switchSidebarView(); }} aria-label="Otevřít menu">
              <FontAwesomeIcon size="2x" className="dashboardMenuButtonIcon" icon={faBars} />
            </button>
            <div className="dashboardSearchbar">
              <input className="dashboardSearchbarInput" type="text" placeholder="Vyhledat" />
              <button className="dashboardSearchbarButton" aria-label="Vyhledat">
                <FontAwesomeIcon size="2x" className="dashboardSearchbarButtonIcon" icon={faSearch} />
              </button>
            </div>
          </div>
          <div className="dashboardSection1b">
            <div className="dashboardUserProfile">
              <div className="dashboardUserProfileButton">
                <FontAwesomeIcon size="2x" className="dashboardUserProfileIcon" icon={faUserAlt} />
              </div>
              <div className="dashboardUserProfileUsername">Admin</div>
              <FontAwesomeIcon size="2x" className="dashboardUserProfileArrow" icon={faChevronDown} />
            </div>
          </div>
        </div>
        <div className="dashboardSection2">
          {this.getStatItem("newUsers", "Nových Uživatelů", extraCurr.uniqueUsers, extraLast.uniqueUsers)}
          {this.getStatItem("views", "Zobrazení", extraCurr.views, extraLast.views)}
          {this.getStatItem("clicks", "Prokliků")}
          {this.getStatItem("shares", "Sdílení")}
        </div>
        <div className="dashboardSection3">
          <div className="dashboardSection3a">
            <div className="dashboardSectionUsersGraphWrapper">
              <div className="dashboardSectionUsersSection1a">
                <div className="dashboardSectionUsersSection2a" style={{ width: "calc(100% - " + (this.props.mainState.wrapAverageGrowth === true && this.props.mainState.pageWidth < 1035 ? 0 : 120) + "px)" }}>
                  <div className="dashboardSectionUsersGraphTitle">Statistika návštěvnosti</div>
                  <div className="dashboardSectionUsersGraphLabels">
                    <div className="dashboardSectionUsersGraphLabel">
                      <div className="dashboardSectionUsersGraphLabelIcon dashboardSectionUsersGraphLabelIconLast"></div>
                      <div className="dashboardSectionUsersGraphLabelText">Minulý měsíc</div>
                    </div>
                    <div className="dashboardSectionUsersGraphLabel">
                      <div className="dashboardSectionUsersGraphLabelIcon dashboardSectionUsersGraphLabelIconCurrent"></div>
                      <div className="dashboardSectionUsersGraphLabelText">Tento měsíc</div>
                    </div>
                  </div>
                </div>
                {this.props.mainState.wrapAverageGrowth === true && this.props.mainState.pageWidth < 1035 ? null : <div className="dashboardSectionUsersSection2b">
                  <div className="dashboardSectionUsersDifference">
                    <FontAwesomeIcon size="2x" className={"dashboardSectionUsersDifferenceArrow " + (difference < 0 ? "dashboardSectionUsersDifferenceArrowRed" : "dashboardSectionUsersDifferenceArrowGreen")} icon={faLongArrowAltUp} />
                    <div className="dashboardSectionUsersDifferenceValue">{difference}%</div>
                  </div>
                  <div className="dashboardSectionUsersDifferenceTitle">Průměrný nárust</div>
                </div>}
              </div>
              <div className="dashboardSectionUsersSection1b">
                <div className="dashboardSectionUsersGraph">
                  <Line
                    responsive={true}
                    data={{
                      labels: labels,
                      datasets: [{
                        label: "Tento měsíc",
                        data: extraCurr.uniqueUsersByDay,
                        backgroundColor: "rgba(66, 199, 50, 0.75)",
                        pointRadius: 0,
                        pointHitRadius: 40
                      }, {
                        label: "Minulý měsíc",
                        data: extraLast.uniqueUsersByDay,
                        backgroundColor: "rgba(33, 177, 209, 1)",
                        pointRadius: 0,
                        pointHitRadius: 40
                      }]
                    }}
                    options={{
                      maintainAspectRatio: false,
                      scales: {
                        xAxes: [{
                          gridLines: {
                            drawOnChartArea: false
                          }
                        }],
                        yAxes: [{
                          gridLines: {
                            drawOnChartArea: false
                          }
                        }]
                      },
                      legend: {
                        display: false
                      }
                    }}
                  />
                </div>
              </div>
            </div>
            <div className="dashboardSectionUserOverviews">
              {this.getUserOverview({ latestTimestamp: "Naposledy", id: "ID", os: "OS", browser: "Prohlížeč", views: "Dnes", viewsTotal: "Celkem" }, true)}
              {userOverviewItems}
            </div>
          </div>
          <div className="dashboardSection3b">
            <div className="dashboardSectionPlatformsSection1">
              <div className="dashboardSectionPlatformsSection1a">
                <div className="dashboardSectionPlatformsGraphTitle">Podíl zařízení za poslední měsíc</div>
              </div>
              <div className="dashboardSectionPlatformsSection1b">
                <div className="dashboardSectionPlatformsGraphWrapper">
                  <div className="dashboardSectionPlatformsGraph">
                    <Doughnut
                      responsive={true}
                      data={{
                        labels: ["Počítač", "Mobil", "Tablet"],
                        datasets: [{
                          data: [pcUsers, mobileUsers, tabletUsers],
                          backgroundColor: ["#e83c5f", "#3da6e3", "#e0ce2d"]
                        }]
                      }}
                      options={{
                        maintainAspectRatio: false,
                        legend: {
                          display: false
                        }
                      }}
                    />
                  </div>
                </div>
                <div className="dashboardSectionPlatformsOverview">
                  <div className="dashboardSectionPlatformsOverviewItemsWrapper">
                    <div className="dashboardSectionPlatformsOverviewItem">
                      <div className="dashboardPlatformIcon dashboardPlatformPC2"></div>
                      <div className="dashboardSectionPlatformsOverviewItemText dashboardPlatformPC">
                        Počítač ({Math.floor((pcUsers/extraCurr.totalUsers)*100)}%)
                      </div>
                    </div>
                    <div className="dashboardSectionPlatformsOverviewItem">
                    <div className="dashboardPlatformIcon dashboardPlatformMobile2"></div>
                      <div className="dashboardSectionPlatformsOverviewItemText dashboardPlatformMobile">
                        Mobil ({Math.floor((mobileUsers/extraCurr.totalUsers)*100)}%)
                      </div>
                    </div>
                    <div className="dashboardSectionPlatformsOverviewItem">
                    <div className="dashboardPlatformIcon dashboardPlatformTablet2"></div>
                      <div className="dashboardSectionPlatformsOverviewItemText dashboardPlatformTablet">
                        Tablet ({Math.floor((tabletUsers/extraCurr.totalUsers)*100)}%)
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="dashboardSectionPlatformsSection2">
                  <div className="dashboardSectionBrowsersOverview">
                    {browserOverviewItems}
                  </div>
                </div>
            </div>
          </div>
        </div>
        <div className="footer">
          <div className="footerText">
            2021 ©
            <b> Sample Copyright</b>
          </div>
          <a href="/" className="footerLink">Domovská stránka</a>
        </div>
      </div>
    );
  }

  getStatItem(id, title, value, value2) {
    let currentValue = value;
    let lastValue = value2;
    if(value === undefined && value2 === undefined) {
      let stats = this.props.functions.getStats();
      stats.sort((a, b) => { return a.month - b.month; })

      let dataLast = stats[stats.length - 2].data;
      lastValue = dataLast.reduce((acc, curr) => { return acc + curr[id]; }, 0);
      let dataCurr = stats[stats.length - 1].data;
      currentValue = dataCurr.reduce((acc, curr) => { return acc + curr[id]; }, 0);
    }
    let difference = ((currentValue - lastValue)/lastValue)*100;

    return (
    <div className="dashboardStatItem">
      <div className="dashboardStatItemSection1">
        <div className="dashboardStatItemValue">
          {currentValue}
        </div>
        <div className="dashboardStatItemTitle">
          {title}
        </div>
      </div>
      <div className="dashboardStatItemSection2">
        {difference < 0 ? <FontAwesomeIcon size="2x" className="dashboardStatItemArrow" icon={faLevelDownAlt} /> : <FontAwesomeIcon size="2x" className="dashboardStatItemArrow" icon={faLevelUpAlt} />}
        <div className="dashboardStatItemDifference">
          {difference.toFixed(0)}%
        </div>
      </div>
    </div>)
  }

  getExtraData(offset) {
    let stats = this.props.functions.getStats();
    stats.sort((a, b) => { return a.month - b.month; })
    let month = stats[stats.length - offset];

    let viewsByDay = [];
    let uniqueUsers = [];
    let uniqueUsersByDay = [];
    let userOverviews = new Map();

    month.data.forEach(day => {
      let viewsThisDay = 0;
      let uniqueUsersThisDay = 0;
      day.users.forEach(user => {
        viewsThisDay += user.views;

        if(uniqueUsers.includes(user.id) === false) {
          uniqueUsers.push(user.id);
          uniqueUsersThisDay += 1;
        }

        if(userOverviews.has(user.id) === false) {
          let userOverview = { latestTimestamp: user.timestamp, id: user.id, os: user.os, browser: user.browser, platform: user.platform, views: user.views, viewsTotal: user.views }
          userOverviews.set(user.id, userOverview);
        } else {
          userOverviews.get(user.id).latestTimestamp = user.timestamp;
          userOverviews.get(user.id).views = user.views;
          userOverviews.get(user.id).viewsTotal += user.views;
        }
      });

      viewsByDay.push(viewsThisDay);
      uniqueUsersByDay.push(uniqueUsersThisDay);
    })

    let usersByBrowser = new Map();
    let usersByPlatform = new Map();
    Array.from(userOverviews.values()).forEach(userOverview => {
      if(usersByPlatform.has(userOverview.platform) === false) {
        usersByPlatform.set(userOverview.platform, { platform: userOverview.platform, value: 1 });
      } else {
        usersByPlatform.get(userOverview.platform).value += 1;
      }

      if(usersByBrowser.has(userOverview.browser) === false) {
        usersByBrowser.set(userOverview.browser, { browser: userOverview.browser, value: 1 });
      } else {
        usersByBrowser.get(userOverview.browser).value += 1;
      }
    })

    return {
      views: viewsByDay.reduce((acc, curr) => { return acc + curr; }),
      uniqueUsers: uniqueUsersByDay.reduce((acc, curr) => { return acc + curr; }),
      viewsByDay,
      uniqueUsersByDay,
      userOverviews,
      usersByBrowser,
      usersByPlatform,
      totalUsers: userOverviews.size
    }
  }

  getUserOverview(userOverview, header=false) {
    let class1 = "dashboardUserOverview" + (header === true ? " dashboardUserOverviewHeader" : "");
    let class2 = "dashboardUserOverviewField" + (header === true ? " dashboardUserOverviewFieldHeader" : "");
    let date = new Date(userOverview.latestTimestamp * 1000);
    let maxFields = (this.props.mainState.wrapRecentVisits === true ? Math.min(Math.max(Math.ceil((this.props.mainState.pageWidth / 1645) * 6), 0), 6) : 6);

    return <div key={userOverview.id} className={class1}>
      {maxFields < 3 ? null : <div className={class2} style={{ width: "calc(100% / " + (maxFields - 1) + ")" }}>
        {(header === true ? userOverview.latestTimestamp : date.toLocaleDateString('cs-CZ'))}
      </div>}
      {maxFields < 6 ? null : <div className={class2} style={{ width: "calc(100% / " + (maxFields - 1) + ")" }}>
        {userOverview.id}
      </div>}
      {maxFields < 5 ? null : <div className={class2} style={{ width: "calc(100% / " + (maxFields - 1) + ")" }}>
        {this.props.functions.getOSDisplayName(userOverview.os)}
      </div>}
      {maxFields < 1 ? null : <div className={class2} style={{ width: "calc(100% / " + (maxFields - 1) + ")" }}>
        {this.props.functions.getBrowserDisplayName(userOverview.browser)}
      </div>}
      {maxFields < 4 ? null : <div className={class2} style={{ width: "calc(100% / " + (maxFields - 1) + ")" }}>
        {((date.getDay() === new Date().getDay() || header === true) ? userOverview.views : 0)}
      </div>}
      {maxFields < 2 ? null : <div className={class2} style={{ width: "calc(100% / " + (maxFields - 1) + ")" }}>
        {userOverview.viewsTotal}
      </div>}
    </div>
  }

  getBrowserOverview(browserOverview, lastBrowserOverview) {
    let difference = ((browserOverview.value - lastBrowserOverview.value)/lastBrowserOverview.value)*100;
    let class1 = "dashboardBrowserOverviewDifferenceValue" + (difference < 0 ? " dashboardBrowserOverviewDifferenceValueRed" : " dashboardBrowserOverviewDifferenceValueGreen");
    let class2 = "dashboardBrowserOverviewDifferenceArrow" + (difference < 0 ? " dashboardBrowserOverviewDifferenceValueRed" : " dashboardBrowserOverviewDifferenceValueGreen");

    return <div key={browserOverview.id} className="dashboardBrowserOverview">
      <div className="dashboardBrowserOverviewTitle">
        {this.props.functions.getBrowserDisplayName(browserOverview.browser)}
      </div>
      <div className="dashboardBrowserOverviewDifference">
        {difference < 0 ? <FontAwesomeIcon size="2x" className={class2} icon={faSortDown} /> : <FontAwesomeIcon size="2x" className={class2} icon={faSortUp} />}
        <div className={class1}>
          {difference}%
        </div>
      </div>
    </div>
  }

}
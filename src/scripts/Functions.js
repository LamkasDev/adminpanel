export default class Functions {
    constructor(_main) {
        this.mainClass = _main;
    }

    selectView(id) {
        this.mainClass.setState({
            selectedView: id
        })
    }

    switchSidebarView() {
        this.mainClass.setState({
            sidebarCompactView: !this.mainClass.state.sidebarCompactView
        })
    }

    getOSDisplayName(os) {
        switch(os) {
            case "win10":
                return "Windows 10";

            case "linux":
                return "Linux";

            case "ios":
                return "iOS";

            case "win7":
                return "Windows 7";

            case "mac":
                return "macOS";

            case "android":
                return "Android";

            default:
                return os;
        }
    }

    getBrowserDisplayName(browser) {
        switch(browser) {
            case "firefox":
                return "Firefox";

            case "chrome":
                return "Chrome";

            case "safari":
                return "Safari";

            default:
                return browser;
        }
    }

    getStats() {
        return [
        {
            month: 1,
            data: [{
                day: 1,
                users: [{
                    timestamp: 1613312535,
                    id: "100300",
                    os: "win10",
                    browser: "chrome",
                    platform: "pc",
                    views: 22
                },
                {
                    timestamp: 1613312535,
                    id: "100320",
                    os: "linux",
                    browser: "chrome",
                    platform: "pc",
                    views: 42
                },
                {
                    timestamp: 1613312535,
                    id: "100310",
                    os: "win7",
                    browser: "firefox",
                    platform: "pc",
                    views: 69
                }],
                clicks: 150,
                shares: 20
            },
            {
                day: 7,
                users: [{
                    timestamp: 1613312535,
                    id: "100320",
                    os: "linux",
                    browser: "firefox",
                    platform: "pc",
                    views: 64
                },
                {
                    timestamp: 1613312535,
                    id: "100330",
                    os: "win10",
                    browser: "edge",
                    platform: "pc",
                    views: 90
                }],
                clicks: 120,
                shares: 50
            },
            {
                day: 14,
                users: [],
                clicks: 10,
                shares: 0
            },
            {
                day: 30,
                users: [{
                    timestamp: 1613312535,
                    id: "100370",
                    os: "ios",
                    browser: "safari",
                    platform: "mobile",
                    views: 5
                }],
                clicks: 70,
                shares: 20
            }]
        },
        {
            month: 2,
            data: [{
                day: 1,
                users: [{
                    timestamp: 1613222534,
                    id: "100390",
                    os: "linux",
                    browser: "firefox",
                    platform: "pc",
                    views: 14
                }],
                clicks: 200,
                shares: 100
            },
            {
                day: 7,
                users: [{
                    timestamp: 1613222534,
                    id: "100400",
                    os: "android",
                    browser: "chrome",
                    platform: "tablet",
                    views: 200
                }],
                clicks: 120,
                shares: 50
            },
            {
                day: 14,
                users: [{
                    timestamp: 1613312535,
                    id: "100400",
                    os: "android",
                    browser: "chrome",
                    platform: "mobile",
                    views: 1
                },
                {
                    timestamp: 1613222534,
                    id: "100410",
                    os: "win10",
                    browser: "chrome",
                    platform: "pc",
                    views: 20
                },
                {
                    timestamp: 1613312535,
                    id: "100420",
                    os: "win10",
                    browser: "firefox",
                    platform: "pc",
                    views: 223
                }],
                clicks: 15,
                shares: 10
            },
            {
                day: 30,
                users: [{
                    timestamp: 1613312535,
                    id: "100430",
                    os: "ios",
                    browser: "safari",
                    platform: "mobile",
                    views: 22
                }],
                clicks: 20,
                shares: 320
            }]
        }]
    }
}
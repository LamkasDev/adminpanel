import React from 'react';
import * as c from './components/index';

export default class AppViews extends React.Component {
  render() {
    switch(this.props.mainState.selectedView) {
      case "dashboard":
        return <c.Dashboard functions={this.props.functions} mainState={this.props.mainState}  />

      default:
        return null;
    }
  }
}
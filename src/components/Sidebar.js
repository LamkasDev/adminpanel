import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThLarge, faBookmark, faEdit, faTable, faChartBar, faMap, faEnvelope, faCalendarAlt, faFileAlt, faSitemap, faChevronLeft } from "@fortawesome/free-solid-svg-icons";

export default class Sidebar extends React.Component {
  render() {
    return (
      <div className={"sidebar" + (this.props.mainState.sidebarCompactView ? " sidebarCompact" : "")}>
        {this.getHeaderButton(this.props.mainState.sidebarCompactView ? "DA" : "DCSoft - Admin")}
        {this.getSeparator("")}
        {this.getButton(faThLarge, "dashboard", "Nástěnka", (id) => { this.props.functions.selectView("dashboard"); }, false)}
        {this.getSeparator("Moduly")}
        {this.getButton(faBookmark, "items", "Položky", (id) => { this.props.functions.selectView("items"); })}
        {this.getButton(faEdit, "posts", "Příspěvky", (id) => { this.props.functions.selectView("posts"); })}
        {this.getButton(faTable, "tables", "Tabulky", (id) => { this.props.functions.selectView("tables"); })}
        {this.getButton(faChartBar, "graphs", "Grafy", (id) => { this.props.functions.selectView("graphs"); })}
        {this.getButton(faMap, "maps", "Mapy", (id) => { this.props.functions.selectView("maps"); })}
        {this.getSeparator("Sekce")}
        {this.getButton(faEnvelope, "post", "Pošta", (id) => { this.props.functions.selectView("post"); })}
        {this.getButton(faCalendarAlt, "calendar", "Kalendář", (id) => { this.props.functions.selectView("calendar"); }, false)}
        {this.getButton(faFileAlt, "sites", "Stránky", (id) => { this.props.functions.selectView("sites"); })}
        {this.getButton(faSitemap, "structure", "Struktura", (id) => { this.props.functions.selectView("structure"); })}
      </div>
    )
  }

  getHeaderButton(title) {
    return (
      <div className="sidebarHeaderButton">
        <div className="sidebarHeaderButtonText">{title}</div>
      </div>
    )
  }

  getSeparator(title) {
    return (
      (this.props.mainState.sidebarCompactView ?
      null
      :
      <div className="sidebarSeparator">
        <div className="sidebarSeparatorText">{title}</div>
      </div>)
    )
  }

  getButton(icon, id, title, onClick, hasArrow=true) {
    let selectedCSS = (this.props.mainState.selectedView === id ? " selectedSidebar" : "");

    return (
      (this.props.mainState.sidebarCompactView ?
      <button className="sidebarButton" onClick={(e) => { onClick(id, e); }}>
        <div className="sidebarButtonSection2">
          <FontAwesomeIcon size="2x" className={"sidebarButtonArrow" + selectedCSS} icon={icon} />
        </div>
      </button>
      :
      <button className="sidebarButton" onClick={(e) => { onClick(id, e); }}>
        <div className="sidebarButtonSection1">
          <FontAwesomeIcon size="2x" className={"sidebarButtonIcon" + selectedCSS} icon={icon} />
          <div className={"sidebarButtonText" + selectedCSS}>{title}</div>
        </div>
        {hasArrow ?
        <div className="sidebarButtonSection2">
          <FontAwesomeIcon size="2x" className={"sidebarButtonArrow" + selectedCSS} icon={faChevronLeft} />
        </div> : null}
      </button>)
    )
  }
}
import React from "react";
import * as c from './components/index';
import AppViews from './AppViews';
import Functions from "./scripts/Functions";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount = () => {
    //Page dimensions hook
    if(this.state.registeredHooks === false) {
      this.updateWindowDimensions();
      window.addEventListener("resize", () => {
        this.updateWindowDimensions();
      });
    }

    this.setState({
      registeredHooks: true
    });
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }
  
  updateWindowDimensions() {
    if(this.state.wrapSidebar === true && window.innerWidth <= 768) {
      this.setState({ sidebarCompactView: true })
    } else if(this.state.pageWidth < window.innerWidth && window.innerWidth > 768) {
      this.setState({ sidebarCompactView: false })
    }

    this.setState({ pageWidth: window.innerWidth, pageHeight: window.innerHeight });
  }

  state = {
    /* Selected */
    selectedView: "dashboard",

    /* UI */
    sidebarCompactView: false,
    pageWidth: -1,
    pageHeight: -1,
    wrapAverageGrowth: true,
    wrapRecentVisits: true,
    wrapSidebar: true,

    /* Utils */
    functions: new Functions(this),
    registeredHooks: false
  }
 
  render() {
    return (
    <div className="app">
      <div className="appContent">
        <c.Sidebar functions={this.state.functions} mainState={this.state} />
        <AppViews functions={this.state.functions} mainState={this.state} />
      </div>
    </div>
    );
  }
}

export default App;

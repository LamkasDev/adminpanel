<div align="center">
<h1>Admin Panel</h1>
<hr />
<h5>By LamkasDev, made with React, Sass</h5>
<img width="6%" src="https://qtlamkas.why-am-i-he.re/pt74eJ.ico" />
</div>
<hr />

#### How to install:
1) Run <code>yarn</code> in main directory (please no npm aaaaaaa)

#### How to build:
1) Run <code>yarn build</code> in main directory
2) All web assets will be built into <code>/build</code> directory

#### How to test:
1) Run <code>yarn start</code>

#### Settings:
There are some settings that can be changed, like responsivness of the dashboard content found in <code>App.js</code>

#### Dataset:
Dataset is intended to be retrieved from a backend server, and the example is found in <code>Functions.js</code>.

<hr />

#### Preview:
<img src="https://qtlamkas.why-am-i-he.re/v2LNRY.png" />
